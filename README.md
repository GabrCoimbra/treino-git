# Treinamento Git

Repositório criado com o intuito de realizar um breve treinamento abrangendo conhecimentos essenciais relacionados ao Git.

## Etapas

**1 -** Clonando o repositório:

```bash
git clone https://gitlab.com/brunoluann/treino-git
```

**2 -** Inserindo um novo arquivo:

```bash
touch index.html
```

**3 -** Verificando o *status* de alterações:

```bash
git status
```

**4 -** Incluindo o novo arquivo à lista de *commit*:

```bash
git add index.html
```

**5 -** Realizando o *commit* com uma mensagem descritiva:

```bash
git commit -m "Inclusão do arquivo index.html"
```

**6 -** Enviando o *commit* ao repositório remoto:

```bash
git push origin master
```

**7 -** Realizando a captura das alterações do repositório remoto com o objetivo de sincronizar alterações em conflito:

```bash
git pull origin master
```

## Merge

Após realizar o `git add <arquivo(s)>`, e, em seguida, o `git commit`, uma tela surgirá em seu terminal.
Para encerrar esta tela que surgiu, tente uma das seguintes opções abaixo:

**1 -** `ESC` + :wq<br>
**2 -** `CTRL` + X<br>
**3 -** Pressione a tecla `ESC` duas vezes seguidas

## Commits

Para verificar os *commits* já realizados, execute o comando `git log`. A imagem abaixo é um exemplo das informações pertinentes aos *commits* previamente realizados:

![Output no terminal da execução do 'git log'](imagens/git-log.jpg)

Cada *commit* recebe uma identificação única em formato de *hash*, como na `string` abaixo:

`8ef4509773ecc29534291853d1ac20d0232e5410`

Com essa identificação é possível navegar entre diferentes momentos de alterações realizadas em um determinado repositório.

**Exemplo:**<br>

Digamos que você precise analisar o texto contido numa *tag* HTML há dois *commits* anteriores. Para voltar momentaneamente a este momento no passado, execute:

```bash
git checkout 8ef4509773ecc29534291853d1ac20d0232e5410
```

Após feito isso, você estará apto a verificar como o seu repositório era quando este *commit* foi realizado.

Após feita a análise necessária neste cenário hipotético, você precisará voltar ao momento atual do seu repositório. Para isso, volte à branch **master** (ou qualquer outra de sua escolha):

```bash
git checkout master
```

E pronto! Você estará de volta ao "presente"!<br><br>

# Desenvolvimento local

Segue abaixo uma série de informações pertinentes ao *setup* dos
softwares e/ou ferramentas de desenvolvimento a serem instaladas para
uso:

## XAMPP

Em um teste local realizado utilizando o **XAMPP** na versão 7.3.1
demonstrou êxito durante seu funcionamento. A versão testada conta com
o [PHP](https://php.net) na versão 7.3.

[**> Link para download**](https://www.apachefriends.org/pt_br/index.html)

## CodeIgniter

A versão do framework **CodeIgniter** utilizada para testes foi a 3.1.10.

[**> Link para download**](https://github.com/bcit-ci/CodeIgniter/archive/3.1.10.zip)

## GitBash

Por ser uma melhor alternativa ao `CMD` nativo do Windows, o GitBash é
uma ferramenta de linha de comando recomendável devido a sua
flexibilidade. No link de referência disponibilizado abaixo consta a
instalação do Git e o GitBash em sua máquina.

[**> Link para download**](https://git-scm.com/downloads)

# Configuração

O CodeIgniter será a tecnologia que irá demandar uma certa
configuração para que funcione em definitivo.

> As configurações descritas abaixo são voltadas a projetos que estão sendo criados do princípio, ou seja, se você só deseja configurar um ambiente para trabalhar em um projeto já existente, ignore o conteúdo a seguir.

## Biblioteca de encriptação

De início, é provável que você se depare com alguma mensagem de erro
informando que a biblioteca de encriptação que o CodeIgniter faz uso
está ausente no diretório *system* do framework. Para solucionar este
problema, copie o conteúdo existente [neste link do
GitHub](https://raw.githubusercontent.com/kpushpendra81/exam/master/encrypt.php)
ao arquivo `Encrypt.php` existente em `system/libraries`.

## Config

No arquivo `config.php` existente em `application/config`, defina o
`base_url` da aplicação. Exemplo:

```php
$config['base_url'] = 'http://localhost/cms/';
```

## Autoload

Ainda no diretório mencionado acima (seção 'Config'), só que desta vez
no arquivo `autoload.php`, configure os seguintes valores:

**1 -** Definição de bibliotecas utilizadas pelo sistema:

```php
$autoload['libraries'] = array('database', 'email', 'session', 'encrypt');
```

**2 -** Definição do *helper* necessário ao funcionamento das rotas:

```php
$autoload['helper'] = array('url');
```

## HTACCESS

Na raíz do projeto, crie o arquivo `.htaccess` (caso não exista) com o
seguinte conteúdo:

```bash
RewriteEngine on

RewriteCond $1 !^(index.php|uploads/|images|robots\.txt|system|assets)
RewriteCond $1 !^(index\.php|resources|robots\.txt)
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [L,QSA]
```